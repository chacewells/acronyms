<?php

    // Handle Add Action

    if (isset($_GET['action']) && $_GET['action'] == 'Add') {

        $new_key = $_GET['acronym'];
        $new_val = $_GET['definition'];

        // Write to File
        $fh = fopen("data0.txt", 'a');
        fwrite($fh, $new_key . " " . $new_val . "\n");
        fclose($fh);
    }


    // Load Dictionary

    $map = array();

    $handle = fopen("data0.txt", "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {


            $part = explode(" ", $line, 2);
            $key = $part[0];
            $val = $part[1];

            $map[$key] = $val;
        }
    } else {
        echo "Failed to load file";
    }
    fclose($handle);

	//Change Dictionary As Search Is Typed    
	
	//if ($_GET['action] == 'Search[0]'){
	//
	//	  $handle = fopen("data0.txt", "r");
	//	  if ($handle){
	//			while (($line = fgets($handle)) !==false) {
	//
	//				$part = explode(" ", $line, 2);
	//				$key = $part[0];
    //        		$val = $part[1];
	//
    //        		$map[$key] = $val;
    //    							}
    //				} else {
    //    				echo "Failed to load file";
    //						}
    //			fclose($handle);
	
	
	// Handle Search Action

    if (isset($_GET['action']) && $_GET['action'] == 'Search') {

        $search_text = isset($_GET['search']) ? strtoupper($_GET['search']) : ''; //upper case this
        $search_val = isset($map[$search_text]) ? $map[$search_text] : '';

        if ($search_val) {
            echo "<H1>" . $search_text . ': ' . $search_val . "</H1>";
			
        } else {
            echo "<H1>" . $search_text . ': ' . "not found!" . "</H1>";
        }
    }

?>
<!doctype html>
<html>
<head>
    <title>Acronyms!</title>
</head>
<body bgcolor="E6E6FA"> 

<!-- Add Form -->
<div id="add">
    <form>
        <input name="acronym" placeholder="acronym" />
        <input name="definition" placeholder="definition"/>
        <input name="action" type="submit" value="Add"/>
    </form>
</div>
</br>
<!-- Search Form -->
<div id="search">
    <form>
        <input name="search" placeholder="search"/>
        <input name="action" type="submit" value="Search"/>
    </form>
</div>
</br>
<div id="list">

    <!-- Print Acronyms -->
    <?php foreach ($map as $key => $val) { ?>

        <strong><?php echo $key; ?></strong>
        <span><?php echo $val; ?></span>
        <br/>

    <?php } ?>

</div>

</body>
</html>
