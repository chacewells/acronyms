<?php
class StringHeap extends SplHeap {
    protected function compare($value1, $value2) {
    	return -strcmp($value1, $value2);
    }
}

$strheap = new MyStringHeap();
$name_file = fopen('names.txt', 'r');
while ( ($str = fgets($name_file)) != null)
	$strheap->insert($str);
fclose($name_file);

while ( ! $strheap->isEmpty() )
    echo $strheap->extract();
?>
