import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

public class Tabularize {
	public static void main(String... args) throws IOException {
		String fileIn = args[0];
		String fileOut = args[1];
		LinkedList<String> strings = new LinkedList<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(new File(fileIn)))) {
			String str;
			while ( (str = reader.readLine()) != null)
				strings.add(str.replaceFirst(" ", "\t"));
		}

		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(new File(fileOut))))) {
			
			for (String str = strings.removeFirst();
				 str != null;
				 str = strings.removeFirst())
				writer.println(str);
		} catch (NoSuchElementException e) {}

	}
}
